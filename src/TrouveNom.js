import React, { Component } from 'react';

export default class TrouveNom extends Component {

  render() {
    return (
    		<form onSubmit={this.trouveNom.bind(this)}>
            <input type="text" placeholder="trouveNom" ref="trouve"/>
            <input type="submit" />Trouve
          </form>
    );
  }

  trouveNom(event) {
    this.render();
  }

}
